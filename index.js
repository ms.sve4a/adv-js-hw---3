
function buildTable() {
    const table = document.createElement('table');
    table.classList.add('table');

    const ROWS = 30;
    const COLS = 30;

    for(let i = 0; i < ROWS; i++ ) {
        let row = document.createElement('tr');
        for (let j = 0; j < COLS; j++) {
            let col = document.createElement('td');
            col.classList.add('col');
            row.appendChild(col);
        }
        row.classList.add('row');
        table.appendChild(row);
    }

    document.body.addEventListener('click', function(event) {
        if(event.target.matches('.col')) {
            table.classList.remove('active');
            event.target.classList.toggle('active');
        } else {
            table.classList.toggle('active');
        }
    });

    document.body.appendChild(table);

}

buildTable();